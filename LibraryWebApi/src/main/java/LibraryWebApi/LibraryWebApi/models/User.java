package LibraryWebApi.LibraryWebApi.models;

import com.google.gson.annotations.Expose;

public class User
{
    @Expose
    public int Id;

    @Expose
    public String Username;

    @Expose
    public String ElasticId;

    @Expose(serialize = false, deserialize = true)
    public String Password;
}
