package LibraryWebApi.LibraryWebApi.models;

import com.google.gson.JsonElement;

public class SetJsonObject
{

    private final JsonElement response;

    public SetJsonObject(JsonElement response)
    {
        this.response = response;
    }
}
