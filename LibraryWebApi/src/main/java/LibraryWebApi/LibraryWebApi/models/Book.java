package LibraryWebApi.LibraryWebApi.models;

import com.google.gson.annotations.Expose;

import java.util.Date;


public class Book
{
    @Expose
    public int Id;

    @Expose
    public String Name;

    @Expose
    public String Description;

    @Expose
    public String Author;

    @Expose
    public String ImagePath;

    @Expose
    public String ElasticId;

    @Expose
    public Date ReleaseDate;

    @Expose
    public int OwningUserId;
}
