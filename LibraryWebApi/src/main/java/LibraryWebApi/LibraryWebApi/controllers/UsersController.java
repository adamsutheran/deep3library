package LibraryWebApi.LibraryWebApi.controllers;

import LibraryWebApi.LibraryWebApi.models.Book;
import LibraryWebApi.LibraryWebApi.models.User;
import LibraryWebApi.LibraryWebApi.services.ElasticSearchInteract;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collection;

@RequestMapping(path = "/users")
@RestController
public class UsersController implements Filter
{
    private ElasticSearchInteract elasticSearchInteract;

    @Autowired
    public UsersController() throws MalformedURLException
    {
        elasticSearchInteract = new ElasticSearchInteract("http://localhost:9200");
    }

//    THIS IS A DEBUG METHOD SO YOU CAN SEE WHICH USERS YOU HAVE IN ELASTICSEARCH
//    @RequestMapping(method = RequestMethod.GET)
//    Collection<User> getUsers()
//    {
//        return this.elasticSearchInteract.getUsers();
//    }

    @RequestMapping(method = RequestMethod.POST, path = "/login")
    public ResponseEntity Login(@RequestParam String username, @RequestParam String password)
    {
        if((username == null || username.isEmpty()) && (password == null || password.isEmpty()) )
        {
            return new ResponseEntity<>("Either the username or password was incorrect", HttpStatus.BAD_REQUEST);
        }

        User loggedInUser = this.elasticSearchInteract.checkUsernameAndPassword(username, password);

        if(loggedInUser != null)
        {
            return new ResponseEntity(loggedInUser, HttpStatus.OK);
        }
        else if(loggedInUser == null)
        {
            return new ResponseEntity("Incorrect username or password", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity("Error logging in", HttpStatus.INTERNAL_SERVER_ERROR);
    }
/* THIS IS A DEBUG METHOD FOR INSERTING A NEW USER EASILY
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity AddUser(@RequestBody User user)
    {
        if (user == null)
        {
            return new ResponseEntity<>("No book found in request", HttpStatus.BAD_REQUEST);
        }

        Boolean result = this.elasticSearchInteract.AddUser(user);

        if(result)
            return new ResponseEntity<>("Succes!", HttpStatus.CREATED);

        return new ResponseEntity<>("Failed to insert new user", HttpStatus.INTERNAL_SERVER_ERROR);
    }*/




    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With, remember-me");
        chain.doFilter(req, res);
    }


    @Override
    public void init(FilterConfig filterConfig)
    {
    }

    @Override
    public void destroy()
    {
    }
}
