package LibraryWebApi.LibraryWebApi.controllers;


import LibraryWebApi.LibraryWebApi.services.ElasticSearchInteract;
import LibraryWebApi.LibraryWebApi.models.Book;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collection;

@CrossOrigin (origins = "http://localhost:4200/")
@RequestMapping("/library")
@RestController
public class LibraryController implements Filter
{
    private ElasticSearchInteract elasticSearchInteract;

    @Autowired
    public LibraryController() throws MalformedURLException
    {
        elasticSearchInteract = new ElasticSearchInteract("http://localhost:9200");
    }

    @RequestMapping(method = RequestMethod.GET, path = "/test")
    public String test(){
        return "Hello World!";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/books")
    Collection<Book> getBooks()
    {
        return this.elasticSearchInteract.getBooks();
    }

    @RequestMapping(method = RequestMethod.POST, path = "/books")
    public ResponseEntity AddBookToIndex(@RequestBody Book book)
    {
        if (book == null)
        {
            return new ResponseEntity<>("No book found in request", HttpStatus.BAD_REQUEST);
        }

        Boolean result = this.elasticSearchInteract.AddBook(book);

        if(result)
            return new ResponseEntity<>(book, HttpStatus.CREATED);
        else
            return new ResponseEntity<>("Failed to insert new Book", HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @RequestMapping(method = RequestMethod.PUT, path = "/{bookId}/books")
    public ResponseEntity UpdateBook(@RequestBody Book bookToUpdate)
    {
        Boolean result = this.elasticSearchInteract.UpdateBook(bookToUpdate);

        if(result)
            return new ResponseEntity<>(bookToUpdate, HttpStatus.CREATED);
        else
            return new ResponseEntity<>("Failed to insert new Book", HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @RequestMapping(method = RequestMethod.GET, path = "/{bookId}/books")
    public ResponseEntity<Object> getBook(@PathVariable String bookId)
    {
        return new ResponseEntity<Object>("TODO", HttpStatus.OK);
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With, remember-me");
        chain.doFilter(req, res);
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }
}
