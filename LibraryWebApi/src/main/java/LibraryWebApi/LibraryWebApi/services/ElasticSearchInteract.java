package LibraryWebApi.LibraryWebApi.services;

import LibraryWebApi.LibraryWebApi.models.Book;
import LibraryWebApi.LibraryWebApi.models.User;
import com.google.gson.*;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;


public class ElasticSearchInteract
{
    private final String elasticUrl;

    // Constructor
    public ElasticSearchInteract(String elasticUrl) throws MalformedURLException
    {
        this.elasticUrl = elasticUrl;
    }

    /// Summary
    /// A method to add a book from ElasticSearch
    /// Summary
    public boolean AddBook(Book book)
    {
        if (book == null)
        {
            return false;
        }

        try
        {
            URL url = new URL(elasticUrl + "/books-index/_doc/");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            String json = gson.toJson(book);

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(json);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + elasticUrl);
            System.out.println("Post parameters : " + json);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            System.out.println(response.toString());
            if(responseCode >= 200 && responseCode < 300)
                return true;

            return false;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return false;
        }
    }

    /// Summary
    /// A method to get the books from ElasticSearch
    /// Summary
    public Collection<Book> getBooks()
    {
        try
        {
            // setup the URL to connect to and the connection
            URL url = new URL(elasticUrl + "/books-index/_search?pretty=true&q=*.*&filter_path=hits.hits");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");

            // execute and get the response
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            // get the response and buffer it so we can work with it
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();


            System.out.println(response.toString());
            // lets work with the response and get out what we need
            Gson gson = new Gson();
            JsonObject jo = gson.fromJson(response.toString(), JsonObject.class);
           // JsonObject jo = je.getAsJsonObject();
            // first element in the response is called "hits" json object
            JsonObject hitObject = jo.getAsJsonObject("hits");
            // then we needs the json array, also called "hits"
            JsonArray bookArrayJsonObj = hitObject.getAsJsonArray("hits");



            Collection<Book> booksToReturn = new ArrayList<>();
            for (JsonElement jsonElement : bookArrayJsonObj)
            {
                // turn the element into a json object
                JsonObject sourceObj = jsonElement.getAsJsonObject();

                // get the "_source" object out of the json
                JsonObject bookObj = sourceObj.getAsJsonObject("_source");

                // also get the ElasticSearch Id
                JsonPrimitive elasticIdPrim = sourceObj.getAsJsonPrimitive("_id");
                String elasticId = elasticIdPrim.getAsString();

                // try and convert it to our book object
                Book book= gson.fromJson(bookObj, Book.class);
                // IMPORTANT: this needs to be set so we can update it later
                book.ElasticId = elasticId;

                // add the book to an array for returning
                booksToReturn.add(book);
            }

            return booksToReturn;
            //return gson.fromJson(response.toString(), new TypeToken<Collection<Book>>(){}.getType());

        }
        catch(IOException ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    /// Summary
    /// A method to update a book in ElasticSearch
    /// Summary
    public boolean UpdateBook(Book bookDetailsToUpdate)
    {
        try
        {
            // setup the URL to connect to and the connection
            URL url = new URL(elasticUrl + "/books-index/_doc/" + bookDetailsToUpdate.ElasticId);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("PUT");
            con.setRequestProperty("Content-Type", "application/json");

            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            String json = gson.toJson(bookDetailsToUpdate);

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(json);
            wr.flush();
            wr.close();

            // execute and get the response
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'PUT' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            // get the response and buffer it so we can work with it
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();


            //print result
            System.out.println(response.toString());
            if(responseCode >= 200 && responseCode < 300)
                return true;
            return false;
        }
        catch (IOException ex)
        {

        }

        return false;
    }



    /// Summary
    /// Get the users from ElasticSearch
    /// Summary
    public Collection<User> getUsers()
    {
        try
        {
            // setup the URL to connect to and the connection
            URL url = new URL(elasticUrl + "/users-index/_search?pretty=true&q=*.*&filter_path=hits.hits");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");

            // execute and get the response
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            // get the response and buffer it so we can work with it
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();


            System.out.println(response.toString());
            // lets work with the response and get out what we need
            Gson gson = new Gson();
            JsonObject jo = gson.fromJson(response.toString(), JsonObject.class);
            // JsonObject jo = je.getAsJsonObject();
            // first element in the response is called "hits" json object
            JsonObject hitObject = jo.getAsJsonObject("hits");
            // then we needs the json array, also called "hits"
            JsonArray userArrayJsonObj = hitObject.getAsJsonArray("hits");

            Collection<User> usersToReturn = new ArrayList<>();
            for (JsonElement jsonElement : userArrayJsonObj)
            {
                // turn the element into a json object
                JsonObject sourceObj = jsonElement.getAsJsonObject();
                // get the "_source" object out of the json
                JsonObject userObj = sourceObj.getAsJsonObject("_source");
                // try and convert it to our user object
                User user = gson.fromJson(userObj, User.class);
                // add the user to an array for returning
                usersToReturn.add(user);
            }

            return usersToReturn;

        }
        catch(IOException ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    private User getUser(String username)
    {
        try
        {
            // setup the URL to connect to and the connection
            URL url = new URL(elasticUrl + "/users-index/_search"); // ?pretty=true&filter_path=hits.hits
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            Gson gson = new Gson();

            // we have to do this in order for the json to be used correctly in Gson terms

            JsonObject queryObj = (new JsonParser()).parse("{ \"query\" : { \"term\" : { \"Username\" : \"" + username + "\" } } }").getAsJsonObject();

            // convert the whole thing to a json string
            String json = gson.toJson(queryObj);


            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(json);
            wr.flush();
            wr.close();
            // execute and get the response for POST that actually gets
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            // get the response and buffer it so we can work with it
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();


            System.out.println(response.toString());
            // lets work with the response and get out what we need

            JsonObject jo = gson.fromJson(response.toString(), JsonObject.class);
            // JsonObject jo = je.getAsJsonObject();
            // first element in the response is called "hits" json object
            JsonObject hitObject = jo.getAsJsonObject("hits");
            // then we needs the json array, also called "hits"
            JsonArray userArrayJsonObj = hitObject.getAsJsonArray("hits");

            for (JsonElement jsonElement : userArrayJsonObj)
            {
                // turn the element into a json object
                JsonObject sourceObj = jsonElement.getAsJsonObject();
                // get the "_source" object out of the json
                JsonObject userObj = sourceObj.getAsJsonObject("_source");
                // get the elastic ID so that the front end can use it to match books to users
                JsonPrimitive elasticIdPrim = sourceObj.getAsJsonPrimitive("_id");
                String elasticId = elasticIdPrim.getAsString();

                // try and convert it to our user object
                User user = gson.fromJson(userObj, User.class);

                user.ElasticId = elasticId;
                // add the user to an array for returning
                return user;
            }

            return null;

        }
        catch(IOException ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    public User checkUsernameAndPassword(String username, String password)
    {
        // get a user matching the username supplied
        User matchingUser = this.getUser(username);


        if(matchingUser.Username.equals(username))
        {
            if(matchingUser.Password.equals(password))
            {
                return matchingUser;
            }
            return null;
        }

        return null;
    }

    public Boolean AddUser(User user)
    {
        if (user == null)
        {
            return false;
        }

        try
        {
            URL url = new URL(elasticUrl + "/users-index/_doc/");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            String json = gson.toJson(user);

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(json);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + elasticUrl);
            System.out.println("Post parameters : " + json);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            System.out.println(response.toString());
            if(responseCode >= 200 && responseCode < 300)
                return true;

            return false;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return false;
        }
    }
}
